FROM python:3.10
WORKDIR /app
RUN apt update && apt install ffmpeg -y
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY src src
ENTRYPOINT ["python", "-m", "src.main"]