--extra-index-url https://download.pytorch.org/whl/cpu
torch
torchvision
torchaudio
lightning
mlflow
python-dotenv
fastapi
prometheus-fastapi-instrumentator