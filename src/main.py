import os
import tempfile

import mlflow
import torch
import torchaudio
import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.responses import FileResponse
from prometheus_fastapi_instrumentator import Instrumentator

from src.config import NUM_DEMOS, NUM_SAMPLES, SAMPLE_RATE, DEMO_STEPS

load_dotenv()
torch.set_float32_matmul_precision("high")
mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
device = 'cpu'
model = mlflow.pytorch.load_model('models:/audio_diffusion/2').to(device)

app = FastAPI()
Instrumentator().instrument(app).expose(app)


@app.get('/generate')
async def generate():
    with tempfile.NamedTemporaryFile(suffix='.wav', delete=False) as temp_file:
        with torch.no_grad():
            noise = torch.randn((NUM_DEMOS, 2, NUM_SAMPLES)).to(device)

            max_start_time = 234.83733

            durations = []
            start_times = []
            for i in range(NUM_DEMOS):
                durations.append(1)
                start_times.append(i * NUM_SAMPLES / SAMPLE_RATE / max_start_time)
            durations = torch.tensor(durations)
            start_times = torch.tensor(start_times)

            cond = torch.stack((start_times, durations), -1).unsqueeze(-1).to(device)

            fakes = model.sample(noise, cond, DEMO_STEPS, 0)
            fakes = fakes.clamp(-1, 1)
            fakes = fakes.permute(1, 0, 2)
            fakes = fakes.flatten(1)
            fakes = fakes.cpu()

            torchaudio.save(temp_file.name, fakes, SAMPLE_RATE)
            return FileResponse(temp_file.name)


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8000)
