import math

import torch
import torchaudio
from lightning import LightningModule, Callback
from torch.nn import MSELoss
from torch.optim import AdamW
from torch.quasirandom import SobolEngine

from src.config import (
    DEMO_EVERY,
    NUM_DEMOS,
    NUM_SAMPLES,
    SAMPLE_RATE,
    DEMO_STEPS,
    DEMOS_PATH,
    LEARNING_RATE,
)
from src.model.diffusion import DiffusionAttnUnet1D


class AudioGenerationModel(LightningModule):
    def __init__(self):
        super().__init__()

        self.diffusion = DiffusionAttnUnet1D(
            io_channels=2,
            depth=14,
            n_attn_layers=6,
            c_mults=[16, 16, 32, 32] + [64] * 10,
            latent_dim=2,
        )
        self.rng = SobolEngine(1, scramble=True, seed=42)

        self.loss = MSELoss()

    def configure_optimizers(self):
        return AdamW(self.parameters(), lr=LEARNING_RATE)

    def training_step(self, batch, batch_idx):
        waveform, start_time, duration = batch

        cond = torch.stack((start_time, duration), -1).unsqueeze(-1)

        # Draw uniformly distributed continuous timesteps
        t = self.rng.draw(waveform.size(0))[:, 0].to(waveform)

        t = self.get_crash_schedule(t)
        alphas, sigmas = self.get_alphas_sigmas(t)

        # Combine the ground truth audios and the noise
        alphas = alphas[:, None, None]
        sigmas = sigmas[:, None, None]
        noise = torch.randn_like(waveform)
        noised_waveform = waveform * alphas + noise * sigmas
        targets = noise * alphas - waveform * sigmas

        v = self.diffusion(noised_waveform, t, cond)
        loss = self.loss(v, targets)

        self.log("loss", loss, prog_bar=True, on_step=True, on_epoch=True)

        return loss

    def get_crash_schedule(self, t):
        sigma = torch.sin(t * math.pi / 2) ** 2
        alpha = (1 - sigma**2) ** 0.5
        return self.alpha_sigma_to_t(alpha, sigma)

    def alpha_sigma_to_t(self, alpha, sigma):
        """Returns a timestep, given the scaling factors for the clean audio and for
        the noise."""
        return torch.atan2(sigma, alpha) / math.pi * 2

    def get_alphas_sigmas(self, t):
        """Returns the scaling factors for the clean audio (alpha) and for the
        noise (sigma), given a timestep."""
        return torch.cos(t * math.pi / 2), torch.sin(t * math.pi / 2)

    def sample(self, x, cond, steps, eta):
        """Draws samples from a model given starting noise."""
        ts = x.new_ones([x.shape[0]])

        # Create the noise schedule
        t = torch.linspace(1, 0, steps + 1)[:-1]

        t = self.get_crash_schedule(t)

        alphas, sigmas = self.get_alphas_sigmas(t)

        # The sampling loop
        for i in range(steps):
            # Get the model output (v, the predicted velocity)

            v = self.diffusion(x, ts * t[i], cond).float()

            # Predict the noise and the denoised audio
            pred = x * alphas[i] - v * sigmas[i]
            eps = x * sigmas[i] + v * alphas[i]

            # If we are not on the last timestep, compute the noisy audio for the
            # next timestep.
            if i < steps - 1:
                # If eta > 0, adjust the scaling factor for the predicted noise
                # downward according to the amount of additional noise to add
                ddim_sigma = (
                    eta
                    * (sigmas[i + 1] ** 2 / sigmas[i] ** 2).sqrt()
                    * (1 - alphas[i] ** 2 / alphas[i + 1] ** 2).sqrt()
                )
                adjusted_sigma = (sigmas[i + 1] ** 2 - ddim_sigma**2).sqrt()

                # Recombine the predicted noise and predicted denoised audio in the
                # correct proportions for the next step
                x = pred * alphas[i + 1] + eps * adjusted_sigma

                # Add the correct amount of fresh noise
                if eta:
                    x += torch.randn_like(x) * ddim_sigma

        # If we are on the last timestep, output the denoised audio
        return pred


class DemoCallback(Callback):
    def __init__(self):
        super().__init__()

    def on_train_batch_end(self, trainer, pl_module, outputs, batch, batch_idx):
        if trainer.global_step % DEMO_EVERY != 0:
            return
        with torch.no_grad():
            noise = torch.randn((NUM_DEMOS, 2, NUM_SAMPLES)).to(pl_module.device)

            max_start_time = trainer.datamodule.dataset.max_start_time

            durations = []
            start_times = []
            for i in range(NUM_DEMOS):
                durations.append(1)
                start_times.append(i * NUM_SAMPLES / SAMPLE_RATE / max_start_time)
            durations = torch.tensor(durations)
            start_times = torch.tensor(start_times)

            cond = torch.stack((start_times, durations), -1).unsqueeze(-1).to(pl_module.device)

            fakes = pl_module.sample(noise, cond, DEMO_STEPS, 0)
            fakes = fakes.clamp(-1, 1)
            fakes = fakes.permute(1, 0, 2)
            fakes = fakes.flatten(1)
            fakes = fakes.cpu()

            output_path = DEMOS_PATH / f"demo_{trainer.global_step:08}.wav"

            torchaudio.save(output_path, fakes, SAMPLE_RATE)
